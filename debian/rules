#!/usr/bin/make -f

# resolve DEB_HOST_RUST_TYPE
include /usr/share/rustc/architecture.mk

# resolve DEB_DISTRIBUTION
include /usr/share/dpkg/pkg-info.mk

# resolve if release is experimental
DEB_SUITE_EXP = $(filter experimental% UNRELEASED,$(DEB_DISTRIBUTION))

# generate documentation unless nodoc requested
ifeq (,$(filter nodoc,$(DEB_BUILD_OPTIONS)))
MANPAGES = debian/safe.1 debian/safenode.8
endif

# use local fork of dh-cargo and cargo wrapper
PATH := $(CURDIR)/debian/dh-cargo/bin:$(PATH)
PERL5LIB = $(CURDIR)/debian/dh-cargo/lib
export PATH PERL5LIB

# resolve all direct entries in first "package..." section in TODO
VENDORLIBS = $(shell perl -nE \
 '/^(?:\s{2}[*]\s*(?:(package)|(\S+))|\s{4}[*]\s*(\S+))/ or next; \
 $$i++ if $$1; exit if $$i > 1 or $$2; say $$3 if $$3 and $$i;' \
 debian/TODO)

# some tests need writable $HOME
FAKEHOME = $(CURDIR)/debian/fakehome

# known failing tests
TEST_FAILS = \

# test requiring network access
TEST_NET = \
 data_availability_during_churn

# tests hanging endlessly
TEST_HANGS = \
 data_with_churn::data_availability_during_churn \
 sequential_transfers

# generate cargo-checksum file
_mkchecksum = printf '{"package":"%s","files":{}}\n' \
 $$(sha256sum $(or $2,$(dir $1)Cargo.toml) | grep -Po '^\S+') > $1;

comma = ,
_mangle = perl -gpi \
 -e 's/$(1)\W+version = "\K\Q$(subst |,$(comma),$(2))"/$(subst |,$(comma),$(3))"/g;' \
 $(patsubst %,debian/vendorlibs/%/Cargo.toml,$(4))

%:
	dh $@ --buildsystem=cargo

execute_before_dh_auto_configure:
# recreate test folder lost in our git repackaging
	mkdir --parents sn_api/testdata/emptyfolder

override_dh_auto_test: export HOME = $(FAKEHOME)
override_dh_auto_test:
	mkdir --parents "${HOME}"
	$(if $(DEB_SUITE_EXP),\
		dh_auto_test -- --no-fail-fast -- $(addprefix --skip ,$(TEST_NET) $(TEST_HANGS)) || true,\
		dh_auto_test -- --no-fail-fast -- $(addprefix --skip ,$(TEST_NET) $(TEST_HANGS) $(TEST_FAILS)))

# TODO: drop this dirty hack when crate blst is in Debian
override_dh_auto_install:
	dh_auto_install || true

execute_after_dh_auto_install: $(MANPAGES)

override_dh_dwz:

execute_after_dh_auto_clean:
	[ ! -f Cargo.lock.orig ] || mv -f Cargo.lock.orig Cargo.lock

# build manpages
debian/safe.1: debian/%.1: target/$(DEB_HOST_RUST_TYPE)/release/%
	mkdir --parents $(dir $@)
	help2man --section 1 --no-info \
		--name "CLI tool to store and browse data, resolve links etc. on the Safe Network" \
		--output $@ $< \
		|| { $1 --help; false; }
debian/safenode.8: debian/%.8: target/$(DEB_HOST_RUST_TYPE)/release/%
	mkdir --parents $(dir $@)
	help2man --section 8 --no-info \
		--name "service and routing daemon and configuration CLI tool to run a Safe Network node" \
		--output $@ $< \
		|| { $1 --help; false; }

# custom target unused during official build
get-vendorlibs:
# preserve and use pristine Cargo.lock
	[ -f Cargo.lock.orig ] || cp Cargo.lock Cargo.lock.orig
	cp -f Cargo.lock.orig Cargo.lock

# match newer branch of crate base64 v0.21.2
	cargo update --package tonic@0.6.2 --precise 0.9.2
	cargo update --package tonic-build --precise 0.9.2

# preserve needed crates
	cargo vendor --versioned-dirs debian/vendorlibs~
	rm -rf debian/vendorlibs
	mkdir debian/vendorlibs
	cd debian/vendorlibs~ && mv --target-directory=../vendorlibs $(VENDORLIBS)

# tolerate binary files in preserved code
	find debian/vendorlibs -type f ! -size 0 | perl -lne 'print if -B' \
		> debian/source/include-binaries

# accept newer crates
	$(call _mangle,brotli,~3.3.0,3.3.0,self_encryption-0.29.1)
	$(call _mangle,dirs,4.0,>= 4| <= 5,service-manager-0.6.0)

# accept older crates
	$(call _mangle,hashbrown,0.14,>= 0.12.3| <= 0.14,lru-0.12.3)
	$(call _mangle,itertools,0.12.0,>= 0.10.5| <= 0.12,custom_debug_derive-0.6.1)

# mangle to avoid crate webpki-roots
	perl -gpi \
		-e 's/^\[dependencies\.webpki-roots\][^\[]+//;' \
		-e 's/^https(-rustls)?\s*=\s*\K[^\n]+/["https-rustls-probe"]/g;' \
		-e 's/^https-rustls-probe\s*=\s*\K[^\n]+/["once_cell", "rustls", "rustls-native-certs", "rustls-webpki"]/;' \
		debian/vendorlibs/minreq-2.11.0/Cargo.toml

# mangle to avoid mac-only crates
	perl -gpi \
		-e 's/\[target\."\Qcfg(any(target_os = \"macos\", target_os = \"ios\"))\E"[^\n]*(\n[^\[\n][^\n]*)*//g;' \
		debian/vendorlibs/sysinfo-0.30.10/Cargo.toml

# mangle to avoid windows-only crates
	perl -gpi \
		-e 's/\[target\."cfg\(windows\)"[^\n]*(\n[^\[\n][^\n]*)*//g;' \
		debian/vendorlibs/sysinfo-0.30.10/Cargo.toml

# hint at effective licensing for crate blst
	perl -gpi \
		-e 's/\n\Qfn main() {\E\n\K/    println!("dh-cargo:deb-built-using=blst=0={}", env::var("CARGO_MANIFEST_DIR").unwrap());\n\n/g;' \
		debian/vendorlibs/blst-0.3.11/build.rs

# update checksum of mangled crates
	set -e;\
	$(foreach x,\
		blst-0.3.11 \
		custom_debug_derive-0.6.1 \
		lru-0.12.3 \
		self_encryption-0.29.1 \
		service-manager-0.6.0 \
		sysinfo-0.30.10 \
		,\
		$(call _mkchecksum,debian/vendorlibs/$x/.cargo-checksum.json))

# preserve mangled Cargo.lock for use during build
	cp -f Cargo.lock debian/Cargo.lock

	$(eval GONE = $(filter-out 0,\
		$(shell grep -Fxc '      * package is missing' debian/TODO)))
	$(eval PENDING = $(filter-out 0,\
		$(shell grep -Fxc '      * package is pending' debian/TODO)))
	$(eval INCOMPLETE = $(filter-out 0,\
		$(shell grep -Fc '      * package lacks feature' debian/TODO)))
	$(eval BROKEN = $(filter-out 0,\
		$(shell grep -c '      \* package is .*broken' debian/TODO)))
	$(eval UNWANTED = $(filter-out 0,\
		$(shell grep -c '      \* package is unwanted' debian/TODO)))
	$(eval OLD = $(filter-out 0,\
		$(shell grep -Fxc '      * package is outdated' debian/TODO)))
	$(eval AHEAD = $(filter-out 0,\
		$(shell grep -Fxc '      * package is ahead' debian/TODO)))
	$(eval SNAPSHOT = $(filter-out 0,\
		$(shell grep -Fxc '      * package should cover snapshot' debian/TODO)))
	$(eval REASONS = $(strip\
		$(if $(GONE),$(GONE)_missing)\
		$(if $(PENDING),$(PENDING)_pending)\
		$(if $(BROKEN),$(BROKEN)_broken)\
		$(if $(UNWANTED),$(UNWANTED)_unwanted)\
		$(if $(INCOMPLETE),$(INCOMPLETE)_incomplete)\
		$(if $(OLD),$(OLD)_outdated)\
		$(if $(AHEAD),$(AHEAD)_ahead)\
		$(if $(SNAPSHOT),$(SNAPSHOT)_unreleased)))
	$(eval c = ,)
	$(eval EMBEDDED = $(if $(REASONS),\n    ($(subst _,$() ,$(subst $() ,$c_,$(REASONS))))))
	@CRATES=$$(ls -1 debian/vendorlibs | grep -c .);\
	perl -gpi \
		-e 's/FIXME: avoid most possible of embedded \K\d+ crates\n\h+[^\)\n]*\)/'"$$CRATES"' crates$(EMBEDDED)/;' \
		debian/changelog;\
	echo;\
	echo "DONE: embedding $$CRATES crates$(EMBEDDED)";\
	echo

	mv -f Cargo.lock.orig Cargo.lock
